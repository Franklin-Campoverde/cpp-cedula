#include <iostream>
#include <string>
using namespace std;
#include <iostream>
#include <stdlib.h>

int arreglo[10];
int digito;
int sumaDigitos = 0;
int numeroRestar = 0;
int elementoFinal = 0;
int op;

int main()
{
    do
    {
        cout << "Ingrese su número de cédula: \n\n";
        for (int i = 0; i < 10; i++)
        {
            cout << "Ingresar dígito: ";
            cin >> digito;
            arreglo[i] = digito;
        }

        for (int i = 0; i < 9; i++)
        {
            if (i == 0)
            {
                arreglo[i] = arreglo[i] * 2;
                if (arreglo[i] >= 10)
                {
                    arreglo[i] = arreglo[i] - 9;
                }
            }
            if ((i % 2 == 0) && (i != 0))
            {
                arreglo[i] = arreglo[i] * 2;
                if (arreglo[i] >= 10)
                {
                    arreglo[i] = arreglo[i] - 9;
                }
            }
            if ((i % 2 != 0) && (i != 0))
            {
                arreglo[i] = arreglo[i] * 1;
                if (arreglo[i] >= 10)
                {
                    arreglo[i] = arreglo[i] - 9;
                }
            }
        }

        cout << "\nEl número de cédula multiplicado es: \n";
        for (int i = 0; i < 9; i++)
        {
            cout << arreglo[i] << " ";
        }

        for (int i = 0; i < 9; i++)
        {
            sumaDigitos = sumaDigitos + arreglo[i];
        }
        cout << "\nSe suman los 9 primeros dígitos y son multiplicados por 2 los impares y 1 los pares de la cédula: \n";
        cout << sumaDigitos;

        if (sumaDigitos >= 10 && sumaDigitos <= 19)
        {
            numeroRestar = 20 - sumaDigitos;
        }
        if (sumaDigitos >= 20 && sumaDigitos <= 29)
        {
            numeroRestar = 30 - sumaDigitos;
        }
        if (sumaDigitos >= 30 && sumaDigitos <= 39)
        {
            numeroRestar = 40 - sumaDigitos;
        }
        if (sumaDigitos >= 40 && sumaDigitos <= 49)
        {
            numeroRestar = 50 - sumaDigitos;
        }
        if (sumaDigitos >= 50 && sumaDigitos <= 59)
        {
            numeroRestar = 60 - sumaDigitos;
        }
        if (sumaDigitos >= 60 && sumaDigitos <= 69)
        {
            numeroRestar = 70 - sumaDigitos;
        }
        if (sumaDigitos >= 70 && sumaDigitos <= 79)
        {
            numeroRestar = 80 - sumaDigitos;
        }
        if (sumaDigitos >= 80 && sumaDigitos <= 89)
        {
            numeroRestar = 90 - sumaDigitos;
        }
        cout << "\n Comprobación:\n";
        elementoFinal = arreglo[9];
        if (numeroRestar == elementoFinal)
        {
            cout << "La Cédula es correcta.\n";
        }
        if (numeroRestar != elementoFinal)
        {
            cout << "La cédula es incorrecta.\n";
        }
        cout << "¿Desea ingresar otro número? Ingrese:(1/0)";
        cin >> op;
    } while (op == 1 || op == 0);

    system("PAUSE");
    return 0;
}